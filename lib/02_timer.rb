class Timer

  def initialize
    @time_string = ""
    @time = 0
  end

  def second
    @time
  end

  def seconds
    @time
  end

  def time_string
    result_string = ""
    second = @time
    minutes = (@time / 60) % 60
    hour = @time
    while second > 60
      second = second % 60
    end
    if @time >= 3600
      while hour > 60
        hour = hour / 60
      end
    else
      hour = 0
    end
    result_string = "#{padded(hour)}:#{padded(minutes)}:#{padded(second)}"
  end

  def second= second
    @time = second
  end

  def seconds= second
    @time = second
  end

  def padded(digit)
    if digit.to_s.length > 1
      return digit.to_s
    else
      return "0#{digit.to_s}"
    end
  end
end
